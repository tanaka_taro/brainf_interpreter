
var toByte = function(num){
    while(num < 0 || num > 255)
      if(num > 255)
          num %= 256;
      else
          num += 256;
    return num;
}
var print = function(s){
    document.write(s + "<br>\n");
}





class brainF{
    constructor(memorySize){
    this.memorySize = memorySize;
    this.memory = new Array(memorySize);
    for(var i=0;i<this.memory.length;i++)
        this.memory[i] = 0;
    
    this.commandPointer = 0;
    this.memoryPointer = 0;
    this.executeSource = "";

    this.outputStr = "";
    
    this.isEnd = false;
    }

    inputSource(sourceText){
        this.commandPointer = 0;
        this.memoryPointer = 0;

        this.executeSource = sourceText;
    }


    executeAll(){
        while(this.commandPointer < this.executeSource.length)
            this.command();
    }

    command(){
        if(this.commandPointer < this.executeSource.length){
            switch(this.executeSource[this.commandPointer]){
                case "+":
                    this.add();
                    break;
                case "-":
                    this.sub();
                    break;
                case ">":
                    this.shift(true);
                    break;
                case "<":
                    this.shift(false);
                    break;
                case "[":
                    this.loopStart();
                    return;
                    break;
                case "]":
                    this.loopEnd();
                    return;
                    break;
                case ".":
                    this.output();
                    break;
                case ",":
                    this.input();
                    break;
                case "#":
                    this.breakPoint();
                    break;
                default:
                    break;
            }
            this.commandPointer++;
        }else{
            this.isEnd = true;
        }
    }
    
    add(){
        this.memory[this.memoryPointer] = toByte(this.memory[this.memoryPointer] + 1);
    }
    sub(){
        this.memory[this.memoryPointer] = toByte(this.memory[this.memoryPointer] - 1);
    }

    shift(isIncreasing){
        if(isIncreasing){
            this.memoryPointer++;
            if(this.memoryPointer >= this.memorySize)
                this.memoryPointer -= this.memorySize;
        }else{
            this.memoryPointer--;
            if(this.memoryPointer < 0){
                this.memoryPointer += this.memorySize;
            }
        }
    }
    input(){
        this.memory[this.memoryPointer] = prompt("input single character").charCodeAt(0);
    }
    output(){
        this.outputStr += String.fromCharCode(this.memory[this.memoryPointer]);
    }

    loopStart(){
        if(this.memory[this.memoryPointer] == 0)
            this.commandPointer = this.findLoopEnd(this.commandPointer);
        else
            this.commandPointer++;
    }
    loopEnd(){
        if(this.memory[this.memoryPointer] != 0)
            this.commandPointer = this.findLoopStart(this.commandPointer);
        else
            this.commandPointer++;
    }


    findLoopEnd(){
        var count = 0;
        for(var i = this.commandPointer+1;i<this.executeSource.length;i++){
            if(this.executeSource[i] == "[")
                count++;
            else if(this.executeSource[i] == "]")
                if(count != 0)
                    count--;
                else
                    return i+1;
        }
        alert("too many [");
    }

    findLoopStart(){
        var count = 0;
        for(var i=this.commandPointer-1;i>=0;i--){
            if(this.executeSource[i] == "]")
                count++;
            else if(this.executeSource[i] == "[")
                if(count != 0)
                    count--;
                else
                    return i + 1;
        }
    }
    
    breakPoint(){
        this.isEnd = true;
    }

}



